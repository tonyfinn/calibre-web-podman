#!/bin/bash

BUILD_CMD=podman
if [[ ! -z $BUILDER ]]; then
    BUILD_CMD=$BUILDER
fi

./build.sh

mkdir -p books/ config/


echo "Image built, launching now"
$BUILD_CMD run -p "8083:8083" -v "./books:/books" -v "./config:/config"  registry.gitlab.com/tonyfinn/calibre-web-podman:latest
