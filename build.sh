#!/bin/sh

BUILD_CMD=podman
if [ ! -z $BUILDER ]; then
    BUILD_CMD=$BUILDER
fi

echo "Building container with $BUILD_CMD"

if [ $# -gt 0 ]; then
    echo "Building calibre web version $1"
    CALIBRE_WEB_VERSION=$1
    IMAGE_TAG=$1
else
    IMAGE_TAG=$(curl -o - -H "Accept: application/vnd.github.v3+json" \
        https://api.github.com/repos/janeczku/calibre-web/releases/latest | jq -r '.tag_name')
    
    export CALIBRE_WEB_VERSION=$IMAGE_TAG
    echo "Building latest calibre web version: $CALIBRE_WEB_VERSION"
fi

DOCKER_ARGS="--build-arg CALIBRE_WEB_VERSION=$CALIBRE_WEB_VERSION"

if [ $# -gt 1 ]; then
    echo "Building calibre ebook-convert version $1"
    DOCKER_ARGS="--build-arg CALIBRE_VERSION=$1 $DOCKER_ARGS"
else
    echo "Building default calibre ebook-convert version"
fi

$BUILD_CMD build $DOCKER_ARGS -t registry.gitlab.com/tonyfinn/calibre-web-podman:$IMAGE_TAG .
$BUILD_CMD tag registry.gitlab.com/tonyfinn/calibre-web-podman:$IMAGE_TAG registry.gitlab.com/tonyfinn/calibre-web-podman:latest
