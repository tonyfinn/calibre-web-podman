FROM python:3.9.5

WORKDIR /usr/src/calibre-web
VOLUME ["/config", "/books"]
EXPOSE 8083

ARG CALIBRE_VERSION=5.35.0
ARG CALIBRE_WEB_VERSION=0.6.11
ARG KEPUBIFY_VERSION=4.0.3

ENV CALIBRE_INSTALLER_SOURCE_CODE_URL="https://raw.githubusercontent.com/kovidgoyal/calibre/master/setup/linux-installer.py" \
    CALIBRE_DBPATH="/config" \
    CALIBRE_PATH="/books"

RUN \
    sed -i -e's/ main/ main contrib non-free/g' /etc/apt/sources.list && \
    apt-get update -q && \
    apt-get install -y \
    libldap2-dev \
    libsasl2-dev \
    imagemagick \
    libnss3 \
    libxcomposite1 \
    libxi6 \
    libxslt1.1 \
    libldap-2.4-2 \
    libsasl2-2 \
    libxrandr2 \
    unrar && \
    wget -O- ${CALIBRE_INSTALLER_SOURCE_CODE_URL} | \
    python -c \
    "import sys; \
    main=lambda:sys.stderr.write('Download failed\n'); \
    exec(sys.stdin.read()); \
    main(install_dir='/opt', isolated=True, version='${CALIBRE_VERSION}')" && \
    rm -rf /tmp/calibre-installer-cache && \
    curl -SL https://github.com/janeczku/calibre-web/archive/refs/tags/${CALIBRE_WEB_VERSION}.tar.gz \
    | tar -xzC /usr/src/calibre-web && \
    curl -SL https://github.com/pgaskin/kepubify/releases/download/v${KEPUBIFY_VERSION}/kepubify-linux-64bit > /opt/kepubify-linux-64bit && \
    mv calibre-web-*/* . && \
    ls && \
    pip install --target vendor -r requirements.txt -r optional-requirements.txt
CMD [ "python", "./cps.py" ]
