# Calibre Web Podman

## What?

A dockerfile suitable for running calibre-web in podman. It contains kepubify and ebook-convert for converting formats in the calibre-web UI.

## Why?

The two officially recommended container images, from linuxserver.io and Technosoft2000 both use complicated logic to run as non-root users, which doesn't
work so well with rootless podman. This is just a super basic dockerfile to build a container that runs as root in the container, which in rootless
podman equates to running as the user which runs the container.

## How?

### Build container

Requires `jq` and `curl`.

```
./build.sh [<calibre web version>] [<calibre version for ebook-convert>]
```

You may also set the environment variable `BUILDER` to use a compatible CLI to build an image (such as docker or buildah).


### Run container

To build the latest version and run it, use `./run.sh`

To use a prebuilt image, use the image `registry.gitlab.com/tonyfinn/calibre-web-podman:latest` or view [Gitlab](https://gitlab.com/tonyfinn/calibre-web-podman/container_registry) for all available versions. 
